# BeginnerChess
A simple, open source chess engine written in java by a beginner. https://thealaskalinuxuser.wordpress.com

 * A special note of credit goes to https://sites.google.com/site/jonathanwarkentinlogiccrazy/ Jonathan Warkentin's (Logic Crazy) great tutorial of making your own chess engine. A lot of the chess logic comes from his tutorial, and at first, I just found a way to port it to a format that was useful for Android.

 * Now I am attempting to edit it and take my own meager skills as a programmer to the next level.

 * This repo includes the engine within an app. The app currently only allows play of engine vs. engine, to showcase and test the engine itself. My plan is to allow for a "modular" design that others can "drop" into their chess apps that they create (or ones I make myself).

# Work has stopped on this project.
All work has stopped on this engine, as I moved on to work on an improved engine: JustChessEngine. Please see closed issues for more details, but this engine is poorly designed and very slow, thus it was abandoned. Here are some current issues:

* Slow
* Errors when castling
* Errors when promoting
* Errors when performing en passant
* King can castle accross check

This engine was written while I was a complete beginner to programming in Java. Please excuse the mess! :D
